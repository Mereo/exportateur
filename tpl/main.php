<html>
	<head>
		<title><?php echo stripslashes(rawurldecode($_GET["find"]))." - Exportateur de références"?></title>
		<link href='../css/mainStyle.css' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	</head>

	<body>
		<div id="header">
		<div id="logoMain">		
			<a href="../"><img src='../img/logo-main.png'></a>
		</div>
		<div class="searchBox main">
			<form action="" method="get"> 
			<span><input type="text" class="search start" name="find" id="find" size="" value="<?php
																		if (isset($_GET["find"])) {
																				echo stripslashes(rawurldecode($_GET["find"]));
																		} ?>"></span>
																		 
			<span><input type="submit" class="buttonSubmit" value="Recherche"></span> 
			<input type="hidden" name="page" value="1">
			</form> 
			</div>
		</div>

		<div id="searchResultsBody">
		<div class="totalResults"><? echo $data[0]['totalResults'] ?> résultats</div>
		<?php
		if ($data != null) {
			
			for ($i = 0; $i < sizeof($data); $i++) {
				echo '<div class="searchResults">';
				echo '<div class="title"><a href="'.$data[$i]['doiAddress'].'" target="_blank">'.$data[$i]['title'].'</a></div>';
				/*echo '<section class="ac-container">
					  		<div>
					  			<input id="ac-1"  name="accordion-1" type="checkbox" />
					  			<label for "ac-1">Accès hors campus</label>
					  				<article class="medium">
					  				</article>
					  		</div>
					  		<div>
					  			<input id="ac-2"  name="accordion-1" type="checkbox" />
					  			<label for "ac-2">Accès hors campus</label>
					  				<article class="medium">
					  					<a href="https://login.proxy.bib.uottawa.ca/login?url='.$data[$i]['doiAddress'].'"><img src="../img/sfx-button-90.gif"></a>
					  				</article>
					  		</div>
					  </section>'; */
				
				
				
				echo "<div class='citation'>".$data[$i]['citation']."</div>";
				//echo "<strong>Année: </strong>".$data[$i]['year'];
				echo "<div class='doiNum'>".$data[$i]['doiNum']."</div>";			
				echo '<div class="export">
							<FORM METHOD="get" ACTION="../index" target="RefWorksMain">
								<INPUT TYPE="submit" class="buttonGeneral export" VALUE="Exporter" target="RefWorksMain">
								<input type="hidden" name="export" value="'.$data[$i]['doiAddress'].'"
								<input type="hidden" name="page" value="1">
							</FORM>
					</div>';
				echo '<section class="ac-container">
				<div>
					<input id="ac-'.$i.'" name="accordion-1" type="checkbox" />
					<label for="ac-'.$i.'">Accès hors campus</label>
					<article class="ac-small">
						<a href="https://login.proxy.bib.uottawa.ca/login?url='.$data[$i]['doiAddress'].'" target="_blank"><img src="../img/uottawa.png" class="imageLink"></a>
						<a href="https://login.proxy2.lib.umanitoba.ca/login?qurl='.$data[$i]['doiAddress'].'" target="_blank"><img src="../img/usb.png"class="imageLink"></a>
						<a href="https://proxy.cm.umoncton.ca/login?qurl='.$data[$i]['doiAddress'].'" target="_blank"><img src="../img/moncton.png"class="imageLink"></a>
					</article>
				</div>
				
			</section>';
				
				//echo "<div class='export'><a href='"."../?export=".$data[$i]['doiAddress']."' target=\"RefWorksMain\">Exporter vers Refworks</div></a>";
				
				//echo "<A href="http://www.refworks.com/express/ExpressImport.asp?vendor=GReFoPSName&filter=ris%20format&encoding=65001&url="http://mereo.ca/~quake/references/?ris="http://dx.doi.org/10.1787/065817785801"&target="RefWorksMain">Export to RefWorks</A>";
				echo '</div>';
			}
			echo "<div class='line-endSeparator'>line</div>";
			
			//Previous Page
			if ($functions['isPreviousPage']()) {
				echo "<div class='previous active'><a href='".call_user_func($functions['previousPage'],null)."'>Précédent</div></a>";

			}
			else {
				echo "<div class='previous inactive'>Précédent</div>";
			}
			//Next Page
				echo "<div class='pageNumInfo'>Page ".$_GET["page"]."</div>";
				
				echo "<div class='next active'><a href='".call_user_func($functions['nextPage'],null)."'>Suivant</div>";
		}
		?>
		</div>
	</body>

</html>
