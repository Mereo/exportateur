<html>
	<head>
		<title>Exportateur de références</title>
		<link href='../css/mainStyle.css' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	</head>

	<body>
		<script>
		window.onload = function() {
		document.getElementById("find").focus();
		};
		</script>
		
		<div id="logoSearch">
			<div class="logo">
				<img src="../img/logo-fr.png">
			</div>
		
			<div class="searchBox start">
			<form action="" method="get"> 
			<span><input type="text" class="search start" name="find" id="find" size="" value="<?php
																		if (isset($_GET["find"])) {
																				echo rawurldecode($_GET["find"]);
																		} ?>"></span>
																		 
			<span><input type="submit" class="buttonSubmit" value="Recherche"></span> 
			<input type="hidden" name="page" value="1"> 
			</form> 
			</div>
		</div>
		
			<?php
		if ($data != null) {
			for ($i = 0; $i < sizeof($data); $i++) {
				echo "<strong>Titre: </strong>".$data[$i]['title'];
				echo "<br />";
				echo "<strong>Référence: </strong>".$data[$i]['citation'];
				echo "<br />";
				echo "<strong>Année: </strong>".$data[$i]['year'];
				echo "<br />";
				echo "<strong>DOI: </strong>"."<a href='".$data[$i]['doiAddress']."'>".$data[$i]['doiAddress']."</a>";			
				echo "<br />";
				echo "<a href='"."../?export=".$data[$i]['doiAddress']."' target=\"RefWorksMain\">Exporter vers Refworks</a>";
				echo "<p>";
			}
		}
		?>
	</body>

</html>
