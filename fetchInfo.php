<?php
	define("NUMBERARTICLE",10);
	
	class Fetcher {
		
		private function highlight($text, $words) {
						preg_match_all('~\w+~', $words, $m);
						if(!$m)
							return $text;
						$re = '~\\b(' . implode('|', $m[0]) . ')\\b~i';
						return preg_replace($re, '<strong>$0</strong>', $text);
		}
		
		public function fetchGeneralInfo($searchQuery, $numberOfArticles, $page) {
			
			$crossRefUrl = "http://search.crossref.org/dois";
			
			$ch = curl_init();
			//echo urlencode($searchQuery);
			curl_setopt($ch, CURLOPT_URL, $crossRefUrl."?q=".rawurlencode($searchQuery)."&page=".$page."&rows=".$numberOfArticles."&header=true");
		
			//receive server response
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //tres important pour recevoir le texte
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$server_output = curl_exec($ch);
			curl_close($ch);
			$jsonArray = json_decode($server_output, true);
			$jsonArrayReturned = $jsonArray['items'];
			$toReturn;
			//var_dump($jsonArray);
			//function that puts words in bold
			for ($i = 0; $i < sizeof($jsonArrayReturned); $i++) {
				//get the doi number
				$doiNum = parse_url($jsonArrayReturned[$i]['doi'], PHP_URL_PATH);
				$doiNum = substr($doiNum, 1);
				//code to highlight the searched words
				$fullCitation = $jsonArrayReturned[$i]['fullCitation'];
				$boldCitation = $this->highlight($fullCitation, $searchQuery);
				$toReturn[$i] = array(
									'doiAddress'=>$jsonArrayReturned[$i]['doi'],
									'doiNum'=>$doiNum,
									'title' =>$jsonArrayReturned[$i]['title'],
									'citation'=>$boldCitation,
									'year'=>$jsonArrayReturned[$i]['year'],
									'totalResults'=>$jsonArray['totalResults']);
			}
			//var_dump($toReturn);
			return $toReturn;
		}
	
		public function fetchSpecificInfo($doiUrl) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $doiUrl);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); //tres important pour recevoir le texte
			curl_setopt($ch, CURLOPT_HTTPHEADER,array(
												"Accept: application/x-research-info-systems"
												));
			//return server response
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
			//execute curl
			$server_output = curl_exec($ch);
			curl_close($ch);
			return $server_output;
		}
	}
	
	class Worker {
		//class variables
		private $viewer;
		private $fetcher;
		
		public function __construct() {
			$this->fetcher = new Fetcher();
			$this->viewer = new Viewer();
		}
		
		public function find($searchQuery, $numberOfArticles, $page) {
			//If the number of Articles is null
			if (is_null($numberOfArticles)) {
				$numberOfArticles = NUMBERARTICLE;
			}
			
			if ( !is_numeric($page) ) {
				$page = 1;
				$_GET["page"] = 1;
			}
			$jsonArrayReturned = $this->fetcher->fetchGeneralInfo($searchQuery, $numberOfArticles, $page);
			//We're going to put the defined functions into an array
			$functionArray = array(
				'isPreviousPage' => function() {
										if ($_GET["page"] <= 1 || !is_numeric($_GET["page"])) {
											return false;
										}
										else {
											return true;
										}
									},			
				
				'isNextPage' => function() {
									if ($_GET["page"] != 1) {
										return false;
									}
									else {
										return true;
									}
								},					
				
				'previousPage' => function() {
									$queryArray = $_GET;
									$queryArray["page"] = (int)$_GET["page"] -1;
									return "?".http_build_query($queryArray);
								},
				
				'nextPage' => function() {
								$queryArray = $_GET;
								$queryArray["page"] = $_GET["page"] +1;
								return "?".http_build_query($queryArray);
							},
				'setCookies' => function() {
					}			
				);
			
			$this->viewer->output('find',$jsonArrayReturned,$functionArray,null);
		}
		
		public function export($doi) {
			$vendor = rawurlencode("Exportateur de références");
			$filter = rawurlencode("RIS Format");
			$encoding = 65001;
			$serviceURL = "http://refworks.com/refworks/expressimport.asp?";
			$actual_link = "http://$_SERVER[HTTP_HOST]";
			$toReturn = $serviceURL."vendor=".$vendor."&filter=".$filter."&encoding=".$encoding."&url=".$actual_link."/?ris=".$doi;
			//echo $toReturn;
			$this->viewer->output("export",$toReturn,null,null);
		}

		public function getRis($doi) {
			$ris = $this->fetcher->fetchSpecificInfo($doi);
			$this->viewer->output('ris',$ris,null,null);
		}
		
		public function isNextPage() {
			if ($_GET["page"] != 1) {
				return true;
			}
			else {
				return false;
			}
		}
		
		public function previousPage() {
			$url=$_GET["find"];
			return "../?find=".rawurlencode($url)."&page=".$_GET["page"]--;
		}
		
		public function nextPage() {
			$url = $_GET["find"];
			return "../?find=".rawurlencode($url)."&page=".$_GET["page"]++;
		}
		
		public function invoke() {
			if (empty($_GET)) {
				$this->viewer->output('mainPage',null,null,null);
			}
			if (isset($_GET["find"]) && isset($_GET["page"])) {
					$this->find(htmlspecialchars($_GET["find"]),NUMBERARTICLE,$_GET["page"]);
			}
			if (isset($_GET["find"])) {
					$this->find(htmlspecialchars($_GET["find"]),NUMBERARTICLE,1);
			}
			if (isset($_GET["ris"])) {
					$this->getRis($_GET["ris"]);
			}
			if (isset($_GET["export"])) {
					$this->export($_GET["export"]);
			}
		}
	}
	
	class Viewer {  
		private $worker;
		private $tplMainStart="tpl/mainStart.php";
		private $tplMain = "tpl/main.php";
		private $tplRis = "tpl/ris.php";
		private $tplExport = "tpl/export.php";
		
		public function __construct() {
			//$this->worker = $worker;
		}
		
		public function output($action, $data, $functions, $cookies) {
			switch ($action) {
				case "find" :
					require_once($this->tplMain);
					break;
				case "mainPage" :
					require_once($this->tplMainStart);
					break;
				case "ris" :
					require_once($this->tplRis);
					break;
				case "export" :
					require_once($this->tplExport);
					break;
			}
		}
	}
?>
