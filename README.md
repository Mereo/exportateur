# README #

### [Exportateur (Scientific References Exporter)](http://exportateur.codinginsanity.com) ###

This PHP web application allows researchers to search for scientific articles using the [DOI](http://dx.doi.org/) database and exports the references to the [RefWorks](http://www.refworks.com/refworks2/) database. This repository is public with the permission from my old job.

[View the PHP application](http://exportateur.codinginsanity.com). It's currently hosted on my FreeBSD DigitalOcean cloud server.